
\begin{code}
open import RATH.Level
-- open import RATH.PropositionalEquality using (≡-refl)
open import Algebra
open import Algebra.Structures
open import Data.Nat
open import Data.Product using (proj₁ ; proj₂)
open import Relation.Binary.Poset.Calc          using (module PosetCalc)
  
module FiniteCalculus {a ℓ : Level}
  {S : Set a} {_≈_ : S → S → Set ℓ} {_+_ _*_ : S → S → S} { - : S → S } { #0 #1 : S }
  (isRing : IsRing _≈_ _+_ _*_ - #0 #1)
  where

open IsRing isRing

open import Relation.Binary.Setoid.Utils
open Setoid′ ((record { Carrier = S ; _≈_ = _≈_ ; isEquivalence = isEquivalence }))
  hiding(_≈_ ; Carrier)
open SetoidCalc ((record { Carrier = S ; _≈_ = _≈_ ; isEquivalence = isEquivalence }))

⊕-cong₁₂ : ∀{x y y′ z} → y ≈ y′ → (x + y) + z ≈ (x + y′) + z
⊕-cong₁₂ {x}{y}{y′}{z} y≈y′ = +-cong (+-cong ≈-refl y≈y′) ≈-refl

⊕-cong₂ : ∀{x y y′} → y ≈ y′ → x + y ≈ x + y′
⊕-cong₂ {x}{y}{y′} eq = +-cong ≈-refl eq


Carrier = S

--
-- repeated ⊕
_·_ : ℕ → Carrier → Carrier
zero · x = ε
suc n · x = (n · x) ⊕ x

Δ : (Carrier → Carrier) → (Carrier → Carrier)
Δ f = λ x → f(x ⊕ inc) ⊝ f x

Δ-identity : ∀{x} → Δ(λ x → x)(x) ≈ inc
Δ-identity {x} =  ≈-begin
    Δ(λ x → x)(x)
  ≈⟨⟩
    (x ⊕ inc) ⊝ x
  ≈⟨ {! ⊕ commutative and assoc !} ⟩
    inc ⊕ x ⊝ x
  ≈⟨  ⊕-cong₂  ( (proj₂ inverse) x )  ⟩
    inc ⊕ ε
  ≈⟨ (proj₂ identity) inc ⟩
    inc
  □ 

Δ-constant : ∀{c x} → Δ(λ x → c)(x) ≈ ε
Δ-constant {c}{x} =  ≈-begin
    Δ(λ x → c)(x)
  ≈⟨⟩
      c ⊝ c 
  ≈⟨  (proj₂ inverse) c ⟩
     ε
  □ 

_^_ : Carrier → ℕ → Carrier
x ^ zero = ε
x ^ suc n = x ⊕ (x ⊝ inc) ^ n

^-cong : ∀{x y n} → x ≈ y → x ^ n ≈ y ^ n
^-cong = {!!}

D^ : ∀ {x n} → Δ(λ x → x ^ n)(x) ≈ {!((suc n) · inc) ⊕ (x ^ n)!}
D^ {x}{zero} =  ≈-begin
    Δ(λ x → x ^ zero)(x)
  ≈⟨⟩
     Δ(λ x → ε)(x) 
  ≈⟨ Δ-constant ⟩
     ε
  □  
D^ {x}{suc n} =   ≈-begin
    Δ(λ x → x ^ (suc n))(x)
  ≈⟨⟩
     ((x ⊕ inc) ^ (suc n)) ⊝ (x ^ (suc n)) 
  ≈⟨⟩
      ((x ⊕ inc) ⊕ ((x ⊕ inc) ⊝ inc) ^ n) ⊕ (x ^ (suc n))⁻¹
  ≈⟨ ⊕-cong₁₂ (^-cong (⊕-assoc x inc inc)) ⟩
      ((x ⊕ inc) ⊕ (x ⊕ inc ⊝ inc) ^ n) ⊝ (x ^ (suc n))
  ≈⟨ ⊕-cong₁₂ (^-cong (proj₂ inverse inc)) ⟩
           ((x ⊕ inc) ⊕ (x ⊕ ε) ^ n) ⊝ (x ^ (suc n))
  ≈⟨ ⊕-cong₁₂ (^-cong ((proj₂ identity) x)) ⟩
           ((x ⊕ inc) ⊕ x ^ n) ⊝ (x ^ (suc n))
  ≈⟨ ⊕-assoc _ _ _ ⟩
           (x ⊕ inc) ⊕ (x ^ n) ⊝ (x ^ (suc n))
  ≈⟨ {!!} ⟩
           (x ⊕ inc) ⊕ (x ^ n) ⊝ (x ⊕ (x ⊝ inc) ^ n)
  ≈⟨ {!!} ⟩
           (x ⊕ inc) ⊕ (x ^ n) ⊕ x ⁻¹ ⊕ ((x ⊝ inc) ^ n) ⁻¹  
  ≈⟨ {! ⊕ commutative !} ⟩
          (x ⊕ inc)  ⊕ x ⁻¹ ⊕ (x ^ n) ⊕ ((x ⊝ inc) ^ n) ⁻¹  
  ≈⟨ {! assoc and double negation !} ⟩
          ( (x ⊕ inc)  ⊕ x ⁻¹ ) ⊕ (x ^ n) ⊕ ((x ⊝ inc) ^ n) ⁻¹  
  ≈⟨⟩
    Δ(λ x → x)(x) ⊕ (x ^ n) ⊕ ((x ⊝ inc) ^ n) ⁻¹
  ≈⟨ ∙-cong (∙-cong Δ-identity ≈-refl) ≈-refl ⟩
    inc           ⊕ (x ^ n) ⊕ ((x ⊝ inc) ^ n) ⁻¹
  ≈⟨ {! recrusion !} ⟩
    inc ⊕ 
  □ 

\end{code}
