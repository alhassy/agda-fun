\documentclass{article}

%{{{ header
\usepackage[hmargin=20mm,top=12mm,bottom=20mm]{geometry}

% unicode stuff
\usepackage[utf8x]{inputenc}
\usepackage{ucs}

\usepackage{multicol}
\usepackage[round]{natbib} % replaces {harvard}
\usepackage{etex}

\usepackage{hyperref}

%{{{ control sequences for lhs2TeX

%include agda.fmt

\newcommand{\textSigma}{$\Sigma$}
\newcommand{\textlambda}{$\lambda$}
\newcommand{\textalpha}{$\alpha$}
\renewcommand{\textbeta}{$\beta$}
%}}}

\begin{document}
%% \centerline{\sc \large Notes on X}
%% \vspace{.5pc}
%% \centerline{\sc Musa Alhassy}
%% \centerline{\it (rough draft)}
%% \vspace{2pc}
%% These rough notes are what I am learning about X
%% \tableofcontents
%}}}

%%% READ ME: copy this, open (git / mingw32) terminal, right-click window top fram, edit -> paste.
%%%
%%% NAME=reader-adjunction ; lhs2TeX --agda $NAME.lagda > $NAME.tex && pdflatex $NAME.tex
%%% C-\  :: toggle (Agda) input method, ie turns \ active/passive.

\centerline{\sc Adjoints for Reader and CoReader Monads}
Set up:
\begin{code}
open import RATH.PropositionalEquality
open import RATH.Data.Product using (Σ∶• ; _,_ ; _×_ ; proj₁ ; proj₂)
open import Relation.Binary.Poset.Calc using (module PosetCalc)
module reader-adjunction where
\end{code}

Assume we have an object of interest,
\begin{code}
module _ 
  (R : Set)
  where
\end{code}
We ignore levels for simplicity.

Besides |Set|, the other category of interest is
a slice category of objects over |R|:
\begin{code}
  Set/R : Set₁
  Set/R = Σ A ∶ Set • (A → R)
\end{code}
whose morphisms, denoted with longer arrows, are commuting triangles:
\begin{code}
  _⟶_ : Set/R → Set/R → Set
  (A , p) ⟶ (B , q) = Σ f ∶ (A → B) • ({a : A} → q(f a) ≡ p a)
\end{code}

Now, the left-adjoint:
\begin{code}
  R* : Set → Set/R
  R* A =  (R × A) , proj₁ -- : R × A → R
\end{code}
and the right-adoint:
\begin{code}
  ∏R : Set/R → Set
  ∏R (A , q) = Σ f ∶ (R → A) • ({r : R} → q(f r) ≡ r) -- × ({a : A} → f(q a) ≡ a)
\end{code}

Now for the main part,
\begin{code}
  lad : {A : Set}{B : Set/R} → (R* A ⟶ B) → (A → ∏R B)
  lad {A} {B , q} (f , qf≈fst) a = (λ(r : R) → f (r , a)) , (λ{r : R} →  ≡-begin
      q( f (r , a))
    ≡⟨  qf≈fst  ⟩
      r
    ≡∎  )
\end{code}
and conversely,
\begin{code}
  rad : {A : Set} {B : Set/R} → (A → ∏R B) → (R* A ⟶ B)
  rad {A} {B , q} f = (λ r,a → proj₁ (f (proj₂ r,a)) (proj₁ r,a)) , (λ {ra} → 
    let r    = proj₁ ra
        a    = proj₂ ra
        fa   = proj₁ (f a)
        qf≈1 = proj₂ (f a)
    in
    ≡-begin
      q (fa r)
    ≡⟨  qf≈1  ⟩
      r
    ≡⟨⟩ -- by definition
      proj₁ ra
    ≡⟨⟩ -- by definition
      proj₂ (R* A) ra
    ≡∎)
\end{code}

Now the fact that these two form a bijection (of hom-sets), the most
difficult part, is left to the reader ;)

\end{document}

% Local Variables:
% folded-file: t
% eval: (fold-set-marks "%{{{ " "%}}}")
% eval: (fold-whole-buffer)
% fold-internal-margins: 0
% end:
