\documentclass{article}

%{{{ header
\usepackage[hmargin=20mm,top=12mm,bottom=20mm]{geometry}

% unicode stuff
\usepackage[utf8x]{inputenc}
%% \usepackage{ucs}

\usepackage{multicol}
\usepackage[round]{natbib} % replaces {harvard}
\usepackage{etex}

\usepackage{hyperref}

%{{{ control sequences for lhs2TeX

%include agda.fmt

\newcommand{\textSigma}{$\Sigma$}
\newcommand{\textlambda}{$\lambda$}
\newcommand{\textalpha}{$\alpha$}
\renewcommand{\textbeta}{$\beta$}
%}}}

\begin{document}

%% \centerline{\sc \large Notes on X}
%% \vspace{.5pc}
%% \centerline{\sc Musa Alhassy}
%% \centerline{\it (rough draft)}
%% \vspace{2pc}
%% These rough notes are what I am learning about X
%% \tableofcontents
%}}}

%%% READ ME: copy this, open (git / mingw32) terminal, right-click window top fram, edit -> paste.
%%%
%%% NAME=domainSG ; lhs2TeX --agda $NAME.lagda > $NAME.tex && pdflatex $NAME.tex
%%% C-\  :: toggle (Agda) input method, ie turns \ active/passive.

\centerline{\sc Fomalised Domain Semigroups}

\begin{code}
open import RATH.Level
-- open import RATH.PropositionalEquality using (≡-refl)
open import Algebra
open import Algebra.Structures
open import Data.Nat
open import Data.Product using (proj₁ ; proj₂)
open import Relation.Binary.Poset.Calc          using (module PosetCalc)

module domainSG where
\end{code}

\section{Introduction}

The purpose of this document is to yield mechanised formal proofs for results in
the theory of domain semigroups. We follow Peter Jipsen's paper, at
\url{http://math.chapman.edu/~jipsen/talks/RelMiCS2009/DesharnaisJipsenStruthDomainSemigroups20090719.pdf}.
While his at \emph{automated} proofs, ours will be mechanised.

Why study semigroups with a notion of domain?
Their ubquitity of-course! A few instances include:
\begin{itemize}
\item relation algebras, or allegories, where domain and range may be defined
\item categories with a one-sort approach via domain and range operators
---that this is not the standard approach may be due to the unfamilairty of
domain semigroups!
\item programming: domain and range occur as pre- and post-conditions
\end{itemize}

As the first as the last are the more popular settings of this theory, we will
use their `multiplication' operation, |_⨾_|, in the definition.
Moreover, we take the constructive approach and so require a setoid structure as well:
\begin{code}
record IsDomainSemigroup {a ℓ} {A : Set a} (_≈_ : A → A → Set ℓ)
  (_⨾_ : A → A → A) (δ : A → A) : Set (a ⊍ ℓ) where
  
  field
    isSemigroup : IsSemigroup _≈_ _⨾_
    δ-cong : {x y : A} → x ≈ y → δ x ≈ δ y
    ax1 : {x   : A} → (δ x ⨾ x)   ≈ x
    ax2 : {x y : A} → δ(x ⨾ y)    ≈ δ(x ⨾ δ y)
    ax3 : {x y : A} → δ(δ x ⨾ y)  ≈ (δ x ⨾ δ y)
    δ-⨾-sym : {x y : A} → (δ x ⨾ δ y) ≈ (δ y ⨾ δ x)

  open import Relation.Binary.Setoid.Utils
  open Setoid′(record
    { Carrier = A ; _≈_ = _≈_ ; isEquivalence = IsSemigroup.isEquivalence isSemigroup })
    using()
  open SetoidCalc (record
    { Carrier = A ; _≈_ = _≈_ ; isEquivalence = IsSemigroup.isEquivalence isSemigroup })

  δ-sym : {x y : A} → δ(x ⨾ y) ≈ δ(y ⨾ x)
  δ-sym {x} {y} = ≈-begin
      δ(x ⨾ y)
    ≈⟨ ax2 ⟩
      δ(x ⨾ δ y)
    ≈⟨ {!!} ⟩
      δ(y ⨾ δ x)
    ≈˘⟨ ax2 ⟩
      δ(y ⨾ x)
    □

  claim : {x : A} → δ(δ x) ≈ δ x
  claim {x} = ≈-begin
      δ(δ x)
    ≈⟨ {!!} ⟩
       δ(δ(δ x) ⨾ x)
    ≈⟨ ax3 ⟩
      δ(δ x) ⨾ δ x
    ≈⟨ ax1 ⟩
      δ x
    □
\end{code}

\end{document}

% Local Variables:
% folded-file: t
% eval: (fold-set-marks "%{{{ " "%}}}")
% eval: (fold-whole-buffer)
% fold-internal-margins: 0
% end:
