\documentclass{article}

%{{{ header
\usepackage[hmargin=20mm,top=12mm,bottom=20mm]{geometry}

% unicode stuff
%% \usepackage[utf8x]{inputenc}
%% \usepackage{ucs}

\usepackage{multicol}
%% \usepackage{natbib}
\usepackage{etex}

\usepackage{hyperref}

%{{{ control sequences for lhs2TeX

\usepackage{/media/musa/Data/RATH-Agda/trunk/ltx/RelNotation}
%include agda.fmt
%include /media/musa/Data/RATH-Agda/trunk/RathAgdaChars.sty
\def\fcmp{\fatsemi}
%format ; = "\; ; \;"
%}}}

\long\def\cmnt#1{}

\begin{document}

 \centerline{\sc \large Undoing Composition: Division in Category Theory}
 \vspace{.5pc}
 \centerline{\sc Musa Al-hassy}
 \centerline{\it (rough draft)}
 \centerline{\sc December 19, 2015}
 \vspace{2pc}
 %% These rough notes are what I am learning about sections and rectractions in category theory.
%% \tableofcontents
%}}}

%%% READ ME: copy this, open (git / mingw32) terminal, right-click window top fram, edit -> paste.
%%%
%%% NAME=CategoryFun ; lhs2TeX --agda $NAME.lagda > $NAME.tex && pdflatex $NAME.tex
%%% C-\  :: toggle (Agda) input method, ie turns \ active/passive.

%{{{ Imports, definition, and examples
First some imports,
\begin{code}
open import RATH.Level               using (_⊍_)
open import RATH.Data.Product        using (Σ∶• ; _×_ ; _,_) renaming (proj₁ to _₁ ; proj₂ to _₂) 
open import Categoric.Category

module CategoryFun where
\end{code}
%}}}

The purpose of this little memo is to investigate properties of inverses in category theory.
We begin by investigating properties of morphisms with pre-inverses ---the `sectionful' morphisms---,
then we dualize to obtain another set of results for free, then we finally investigate when both sided inverses
exist.

First, for convenience, let us define a type for mutual-inhabitedness:
\begin{code}
infix 2 _⇔_
_⇔_ : ∀{ℓ ℓ′} → Set ℓ → Set ℓ′ → Set (ℓ ⊍ ℓ′)
A ⇔ B = (A → B) × (B → A)
\end{code}

%{{{ Two divisions
Our setting is an arbitrary category with objects |Obj|, morphism sets |Mor _ _|,
composition |_⨾_|, and identities |Id {_}|.
\begin{code}
module DivisionOps {i j l}{Obj : Set l}(Cat : Category i j Obj) where

  open Category Cat
\end{code}

A fraction |3 ÷ 5| is characterized as \emph{the} solution $x$ with |5 × x = 3|, since multiplication is
commutative we also have |x × 5 = 3|. Since function composition is not commutative, there are \emph{two}
division problems: given |f, g| there are two possible `fractions \emph{sets}' |f ╱ g| and |g ╲ f| whose elements
are the functions |p| with |p ⨾ g = f| and |q| with |f = g ⨾ q|, respectively. These are fraction-sets since such |p|
and |q| are not necessarily unique, if they exist at all.
\cmnt{
    Give a symbol new Agda TeX binding
      For example, to setup under (residual) in Agda, goto
      |M-x customize-variable agda-input-user-translations| then
      |INS| then for key sequence type |under| then |INS| and
      for string paste |╲|. Now \under yields ╲.

      |infixl 9 _╱_ _╲_|
}

\begin{code}
  infixl 9 _╱_ _╲_
  _╱_ : ∀{A B C} → Mor A C → Mor B C → Set (j ⊍ i)
  _╱_ {A} {B} {C} f g = Σ p ∶ Mor A B • p ⨾ g ≈ f

  _╲_ : ∀{A B C} → Mor C A → Mor C B → Set (j ⊍ i)
  _╲_ {A} {B} {C} g f = Σ q ∶ Mor A B • g ⨾ q ≈ f
\end{code}
For a moment, to get comfortable with the types, let us suppose the division operations
return an actual element, rather than a set ---say by ignoring the proof.
Then |Tgt f = Tgt g ⇒ f ╱ g : Src f ⟶ Src g| and |Src f = Src g ⇒ g ╲ f : Tgt g ⟶ Tgt f|.
That is, the types are read from left-to-right and `over' |╱| uses Src while `under' |╲| uses Tgt.

Of-course the division operators are dual: $|g ╲ f = (f ╱ g)|^{op}$.
%}}}

%{{{ {Division is multiplication with reciprocation aka `sections and retractions'}
\section{Division is multiplication with reciprocation}
Just as division can be regained from multiplication and reciprocation,
e.g., |3 ÷ 5 = 3 × 1/5|, the same can be done with functional-division:
%
%{{{ |sections-determine-╲⇔there-are-sections|
\begin{code}
  --  Every divisor |g ╲ f| is of the form |(Id ╱ g) ⨾ f| iff |Id ╱ g| is non-empty.
  sections-determine-╲⇔there-are-sections : ∀{A B} {g : Mor A B}
      → (∀{C} {f : Mor A C} (q : g ╲ f) → Σ p ∶ Id ╱ g • q ₁ ≈  p ₁ ⨾ f) ⇔ Id ╱ g
  sections-determine-╲⇔there-are-sections {A} {B} {g} =
    (λ uni → (uni {B} {g} (Id , rightId))₁ )                            -- |⇒-direction|
    ,  (λ section {C} {f} q,g⨾q≈f → let p = section ₁   ;   q = q,g⨾q≈f ₁ in -- |⇐-direction|
      section , (≈-begin
         q
        ≈˘⟨ leftId ⟩
           Id ⨾ q
        ≈˘⟨ ⨾-cong₁ (section ₂) ⟩ -- |section ₂ : p ⨾ g ≈ Id|
           (p ⨾ g) ⨾ q
        ≈⟨ ⨾-assoc ⟩
           p ⨾ (g ⨾ q)
        ≈⟨ ⨾-cong₂ (q,g⨾q≈f ₂) ⟩ -- |q,g⨾q≈f ₂ : g ⨾ q ≈ f|
           p ⨾ f
        □
        ))
\end{code}
%}}}
%
Hence, we have all solutions to
division problems once we have solutions to these reciprication problems.
The set |id ╱ g| of pre-inverses of |g| is called the set of \emph{sections for |g|},
while the set |g ╲ id| of post-inverses of |g| is called the set of \emph{rectractions for |g|}.

Dually,
\begin{code}
  -- I.e., every divisor |f ╱ g| is of the form |f ⨾ (g ╲ Id)| iff |g ╲ Id| is non-empty.
  retractions-determine-╱⇔there-are-retractions : ∀{A B} {g : Mor B A}
    → (∀{C} {f : Mor C A} (q : f ╱ g) → Σ p ∶ g ╲ Id • q ₁ ≈ f ⨾ p ₁) ⇔ g ╲ Id
  retractions-determine-╱⇔there-are-retractions {g = g} = (λ uni → (uni (Id , leftId))₁) ,
    (λ rr qq → rr , (rightId ⟨≈˘≈⟩ (⨾-cong₂ (rr ₂) ⟨≈˘≈⟩ (⨾-assoc ⟨≈˘≈⟩ ⨾-cong₁ (qq ₂)))))
\end{code}

\subsection*{A Form of Division Uniqueness}
If both divisions are non-empty, |Id ╱ f| and |f ╲ Id|, then they are identically one unique morphism.
\begin{code}
  -- \cite[p.~54]{conmat}
  inverse-uniqueness : ∀{A B} {f : Mor A B}
    → (s : Id ╱ f) (r : f ╲ Id) → s ₁ ≈ r ₁
  inverse-uniqueness {A} {B} {f} (s , s⨾f≈Id) (r , f⨾r≈Id) = ≈-begin
          s
        ≈⟨ rightId ⟨≈˘≈˘⟩ ⨾-cong₂ f⨾r≈Id ⟩
          s ⨾ (f ⨾ r)
        ≈˘⟨ ⨾-assoc ⟩
          (s ⨾ f) ⨾ r
        ≈⟨ ⨾-cong₁ s⨾f≈Id ⟨≈≈⟩ leftId ⟩
          r
        □
\end{code}
%}}}

%{{{ {Properties of Sectionful Morphisms}
\section{Properties of Sectionful Morphisms}
We now focus on properties of morphsims that have sections only,
since the ones with retractions are dual and so we can re-open
the sectionful properties with the opposite category to obtain
them for free.

Recall, that \[ |p ∈ f ╱ g ⇔ p ⨾ g ≈ f| \]
We call |f ╱ g| a `choice problem' since, in the case of sets,
since for any given |x|, we must 'choose' a |y| such that |g(y) = f(x)|;
then |f ╱ g| is the set of all such choice-functions.

In the category of sets, suppose for each |b : A| we make a tower, or stack, of items
|a : A| upon it such that |f(a) = b|, then we take a `cross-section' of these towers,
i.e., a row of the towers or an element from each row, then this choice is a section
for |f : A → B|. Alternatively, we call such a section a `choice of representatives':
every |a : A| belongs to some tower identified, or represented, by some |b : A|.

Alternatively, suppose that we cut-up the space |A| and identify each region by a |b : A|
so that all the |a : A| in a region satisfy |f(a) = b|. Then a cross-section is nothing
more than a line through the space |A| that picks out one element from each region.


\begin{code}
module SectionfulProps {i j l}{Obj : Set l}(Cat : Category i j Obj) where

  open Category Cat
  open DivisionOps Cat using (_╱_) public
\end{code}

   %{{{ {Identity is a unit for division: $\frac{x}{1} = x$}
\subsection{Identity is a unit for division: $\frac{x}{1} = x$}
Just as 1 is the right unit of division, e.g. |5 ÷ 1|, we have that
|f ╱ id ≅ id ╲ f ≅ { f } |.
\begin{code}
  ╱-Id : ∀ {A B}{f : Mor A B} → (p : f ╱ Id) → p ₁ ≈ f
  ╱-Id (p , p⨾id≈f) = rightId ⟨≈˘≈⟩ p⨾id≈f 
\end{code}
%}}}

   %{{{ section-existence: |∀ y. y ╱ f| is non-empty iff |Id / f| is non-empty.
\subsection{|∀ y. y ╱ f| is non-empty iff |Id / f| is non-empty}
\begin{code}
  section-existence : ∀{A B} {f : Mor A B}
                    → (∀{C}(y : Mor C B) → y ╱ f) ⇔ Id ╱ f

  section-existence {A} {B} {f} = to , from
    where
          to : (∀{C}(y : Mor C B) → y ╱ f) → Id ╱ f
          to f = f Id

          from : Id ╱ f → (∀{C}(y : Mor C B) → y ╱ f)
          from (s , s⨾f≈Id) y = y ⨾ s , (≈-begin
              (y ⨾ s) ⨾ f
           ≈⟨ ⨾-assoc ⟩
              y ⨾ (s ⨾ f)
           ≈⟨ ⨾-cong₂ s⨾f≈Id ⟩
              y ⨾ Id
           ≈⟨ rightId ⟩
              y
           □)

          -- moreover, we have the interesting consequent
          woah : ∀{p : Id ╱ f} → to (from p)₁ ≈ p ₁
          woah {p} = ≈-begin
               to (from p) ₁
            ≈⟨⟩
              Id ⨾ (p ₁)
            ≈⟨ leftId ⟩
              p ₁
            □
\end{code}
\begin{spec}
          -- however the dual, point-wise, |∀ y → (from (to p)) y ₁ ≈ p y ₁| fails
          failure : {p : (∀ {C} (y : Mor C B) → y ╱ f)}
                  → ∀{C}{y : Mor C B} →  (from (to p)) y ₁ ≈ p y ₁
          failure {p} {C} {y} = ≈-begin
               (from (to p)) y ₁
             ≈⟨⟩
               y ⨾ (p Id)₁
             ≈⟨ {!!} ⟩       
               p y ₁ 
             □
\end{spec}
An example scenerio where this fails is in the category of finite sets where we take |B = {1, 2}, A = {3, 4}|
and|y = f = λ _ → 1|; then we can define |p| as follows:
given |x|, if it is |Id| return |λ _ → 3|, otherwise return |λ _ → 4|, both returns belong to |y ╱ f|.
Now, |y ⨾ (p Id) ≈ (λ _ → 1) ⨾ (λ _ → 3) ≈ (λ _ → 3) ≠ (λ _ → 4) ≈ p y|.
%}}}

   %{{{ |sections-determine-╱: p ∈ Id ╱ g ⇒ f ⨾ p ∈  (f ⨾ Id) ╱ g|
\subsection{|sections-determine-╱|}
Okay, so |section-existence| informs us that |╱|-divisions exist precisely when sections exist,
but are the sections enough to determine the |╱-divisions|? Yes, they are!
That is, |p ∈ Id ╱ g ⇒ h ⨾ p  ∈  h ╱ g|.
In-fact, we can generalize this formula as follows:
\begin{code}
  -- c.f. $\frac{a}{b} \times c = \frac{a \times c}{b}$
  -- i.e., |p ∈ f ╱ g ⇒ h ⨾ p  ∈  (h ⨾ f) ╱ g|
  ⨾-╱-fusion : ∀{A B C Z} {h : Mor A B} {f : Mor B C} {g : Mor Z C}
    → (p : f ╱ g) → Σ q ∶ (h ⨾ f) ╱ g • h ⨾ p ₁ ≈ q ₁
  ⨾-╱-fusion {h = h} {f} {g} (p , p⨾g≈f) = (h ⨾ p , (≈-begin
          (h ⨾ p) ⨾ g
        ≈⟨ ⨾-assoc ⟩
          h ⨾ (p ⨾ g)
        ≈⟨ ⨾-cong₂ p⨾g≈f ⟩
           h ⨾ f
        □)) , ≈-refl
\end{code}
Then,
\begin{code}
  -- |sections-determine-╱|
  Id╱g⇒f╱g : ∀{A B C} {f : Mor A C} {g : Mor B C}
             → (p : Id ╱ g) → Σ q ∶ f ╱ g • f ⨾ p ₁ ≈ q ₁
  Id╱g⇒f╱g {f = f} p =
    let ((q , q⨾g≈f⨾Id) , f⨾p≈q) = ⨾-╱-fusion {h = f} p
    in (q , (q⨾g≈f⨾Id ⟨≈≈⟩ rightId)) , f⨾p≈q
\end{code}
What about the converse: |p ∈ Id ╱ g ⟵ f ⨾ p ∈  (f ⨾ Id) ╱ g|? Not always.
\\
A counter-example would be in the category of sets:
let |A = B = C = ℕ, p = Id, f = λ _ → 0| and let |g = λ n → 2*n|,
then |f ⨾ p ⨾ g ≈ f| and so |f ⨾ p ∈ f ╱ g| but |p ∉ Id ╱ g| since |p ≠ g|!
%}}}

\subsection{Repeating Division}
Just as |(a ÷ b) ÷ c ≈ a ÷ (b × c)|, we have
\begin{code}
  -- i.e., |(f ╱ g) ╱ h| and |f ╱ (h ⨾ g)| are mutually inhabited.
  ╱╱ : ∀{A B Y Z} {f : Mor A B} {g : Mor Z B} {h : Mor Y Z} → (Σ p ∶ f ╱ g • p ₁ ╱ h) ⇔ f ╱ (h ⨾ g)
  ╱╱ {f = f} {g} {h} =
    -- |⇒|-direction
    (λ f/g/h → let ((p , p⨾g≈f), (q , q⨾h≈p)) = f/g/h in q , (≈-begin
          q ⨾ h ⨾ g
        ≈⟨ ⨾-assoc ⟨≈˘≈⟩ ⨾-cong₁ q⨾h≈p ⟩
          p ⨾ g
        ≈⟨ p⨾g≈f ⟩
          f
        □)) ,
    -- |⇐|-direction
    (λ f/hg → let (s , s⨾h⨾g≈f) = f/hg in (s ⨾ h , (⨾-assoc ⟨≈≈⟩ s⨾h⨾g≈f)) , s , ≈-refl)
\end{code}

   %{{{ The Category of Sectionful\footnote{A morphisms that has a section.} Morphisms}
\subsection{The Category of Sectionful Morphisms}

Let us call morphisms that have a section as `sectionful'; a more common term is perhaps
`split epimorphism'.

Just as the invertible numbers form a multplicative structure, so too do the functional ones.

Identity is invertible, or `sectionful'.
\begin{code}
  Id-sectionful : ∀{O : Obj} → Id {O} ╱ Id
  Id-sectionful = Id , leftId
\end{code}

The multiplication of invertibles is again invertible,
\begin{code}
  ⨾-sectionful : ∀{A B C} {f : Mor A B} {g : Mor B C}
    → Id ╱ f → Id ╱ g → Id ╱ (f ⨾ g)
  ⨾-sectionful {A} {B} {C} {f} {g} (s , s⨾f≈Id) (q , q⨾g≈Id) = q ⨾ s , (≈-begin
          (q ⨾ s) ⨾ (f ⨾ g)
        ≈⟨ ⨾-assoc ⟨≈≈˘⟩ ⨾-cong₂ ⨾-assoc ⟩
          q ⨾ ((s ⨾ f) ⨾ g)
        ≈⟨ ⨾-cong₂ (⨾-cong₁ s⨾f≈Id ⟨≈≈⟩ leftId) ⟩
          q ⨾ g
        ≈⟨ q⨾g≈Id ⟩
          Id
        □)
\end{code}

That the sectionful morphisms is a category is not too difficult to see.
%}}}

   %{{{ {Has-Section |≈| generally-surjective and Has-Section |⇒| epic }
\subsection{Has-Section |≈| generally-surjective and Has-Section |⇒| epic }
Note that the second part of |section-existence| can be written as a generally-surjectivity claim:
|f| has a section precisely when |∀ y. ∃ x. f ∘ x ≈ y|, i.e.,
\begin{code}
  has-section⇔surjective : ∀{A B} {f : Mor A B}
      → Id ╱ f ⇔ ( ∀ {C} (y : Mor C B) → Σ x ∶ Mor C A • x ⨾ f ≈ y)
  has-section⇔surjective = section-existence ₂ , section-existence ₁
\end{code}
Some things to note,
\begin{itemize}
\item
The |⇒-direction| is \cite[Proposition 1, p.~51]{conmat} and with Agda's help, the converse
seems to also be true.

\item
We qualify this as \emph{general} surjectivity since it uses `generalized elements'.

\item
The case of the the category of sets only requests |y : Mor 1 B|, for a terminal object |1|,
rather than arbitrary |C : Obj|.

\item
Notice that generally-surjectivity |(∀ {C} (y : Mor C B) → Σ x ∶ Mor C A • x ⨾ f ≈ y)|
is just usual set-surjectivity of |(⨾ f) : ∀{C}  → Hom C A → Hom C B|.
\end{itemize}

Let us now turn to the last part in the title:
\emph{epic} morphisms are the pre-cancellable ones
---incidentally, maps with sections are pre-invertible ;)
\begin{code}
  -- \cite[Proposition 2*, p.~53]{conmat}
  sectionful⇒epic : ∀{A B} {f : Mor A B}
    → Id ╱ f
    → ∀{C} {g h : Mor B C} → f ⨾ g ≈ f ⨾ h → g ≈ h
  sectionful⇒epic {A} {B} {f} (s , s⨾f≈Id) {C} {g} {h} f⨾g≈f⨾h = ≈-begin
          g
        ≈⟨ leftId ⟨≈˘≈˘⟩ ⨾-cong₁ s⨾f≈Id ⟩
          (s ⨾ f) ⨾ g
        ≈⟨ ⨾-assoc ⟨≈≈⟩ ⨾-cong₂ f⨾g≈f⨾h ⟩
          s ⨾ (f ⨾ h)
        ≈˘⟨ ⨾-assoc ⟩
          (s ⨾ f) ⨾ h
        ≈⟨ ⨾-cong₁ s⨾f≈Id ⟨≈≈⟩ leftId ⟩
          h
        □
\end{code}
All split morphisms are epic as just shown, but not all epic morphisms are split.
If they were, we would have a form of `axiom of choice'.
%}}}

%}}}

%{{{ {Rectractful Properties}
\section{Rectractful Properties}
Now re-open our previous work but
flipping the divisions around, i.e., using the opposite category, to freely
obtain the corresponding results for maps with retractions.
\begin{code}
module RetractfulProps {i j l}{Obj : Set l}(Cat : Category i j Obj) where

  open Category Cat
  _╲_ : ∀{A B C} → Mor C A → Mor C B → Set (j ⊍ i)
  g ╲ f = (oppositeCategory Cat DivisionOps.╱ f) g -- $|≡ (f ╱ g)|^{op}$

  open SectionfulProps (oppositeCategory Cat) public hiding(_╱_) renaming
    (
      ╱-Id to ╲-Id
        -- |: ∀{A B} {f : Mor A B} → (q : Id ╲ f) → q ₁ ≈ f|
    ; section-existence to retraction-existence 
        -- |: ∀{A B} {f : Mor A B} → (∀{C}(y : Mor A C) → f ╲ y) ⇔ f ╲ Id|
        -- I.e., |∀ y. f ╲ y| is non-empty iff |f ╲ Id| is non-empty.
    ; ⨾-╱-fusion to ╲-⨾-fusion
        -- |: ∀{A B C Z} {h : Mor B A} {f : Mor C B} {g : Mor C Z} → (p : g ╲ f) → Σ q ∶ g ╲ (f ⨾ h) • p ₁ ⨾ h ≈ q ₁|
        -- I.e., |p ∈ g ╲ f ⇒ p ⨾ h  ∈  g ╲ (f ⨾ h)|
    ; Id╱g⇒f╱g to g╲Id⇒g╲f
        -- |: ∀{A B C} {f : Mor C A} {g : Mor C B} → (p : g ╲ Id) → Σ q ∶ g ╲ f • p ₁ ⨾ f ≈ q ₁|
        -- I.e., |retractions-determine-╲|
    ; ╱╱ to ╲╲
        -- i.e., |h ╲ (g ╲ f)| and |(g ⨾ h) ╲ f| are mutually inhabited.
        -- |: ∀{A B Y Z} {f : Mor B A} {g : Mor B Z} {h : Mor Z Y} → (Σ p ∶ g ╲ f • h ╲ p ₁) ⇔ (g ⨾ h) ╲ f|
  
    ; Id-sectionful to Id-retractionful
        -- |: ∀{O : Obj} → Id {O} ╲ Id|
    ; ⨾-sectionful to ⨾-retractionful
        -- |: ∀{C B A} {f : Mor B C} {g : Mor A B} → f ╲ Id → g ╲ Id → (g ⨾ f) ╲ Id|
    ; has-section⇔surjective to has-section⇔co-surjective
        -- |: ∀{A B} {f : Mor A B} → f ╲ Id ⇔ ( ∀ {C} (y : Mor A C) → Σ x ∶ Mor B C • f ⨾ x ≈ y)|
    ; sectionful⇒epic to retractionful⇒monic
        -- |: ∀{A B} {f : Mor A B} → f ╲ Id → ∀{C} {g h : Mor C A} → g ⨾ f ≈ h ⨾ f → g ≈ h|
                                   -- i.e., |→ (⨾ f) : ∀{C}  → Hom C A → Hom C B| is set-injective. 
    )
\end{code}
%}}}

%{{{ thebibliography
\begin{thebibliography}{100}
\bibitem[ConMat97]{conmat}
  F. William Lawvere and Stephen H. Schanuel,
  \emph{Conceptual Mathematics: a first introduction to categories},
  Cambridge University Press, Cambridge, United Kingdom
  1997.
\end{thebibliography}
%}}}

\end{document}

% Local Variables:
% folded-file: t
% eval: (fold-set-marks "%{{{ " "%}}}")
% eval: (fold-whole-buffer)
% fold-internal-margins: 0
% end:
