open import Agda.Primitive

module del where

data ℕ {ℓ : Level} : Set ℓ where
  Z : ℕ {ℓ}
  S : ℕ {ℓ} → ℕ {ℓ}
  ∞ : ℕ
